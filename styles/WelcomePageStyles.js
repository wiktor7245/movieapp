import { StatusBar, StyleSheet } from "react-native";

export default (WelcomePageStyles = StyleSheet.create({
    main:{
        backgroundColor: "#EDEDED",
        flex:1,
        paddingTop: StatusBar.currentHeight
    },

    searchBar:{
        backgroundColor: "white",
        elevation: 10,
        marginBottom: 10
    },

    searchBtnContainer:{
        alignSelf: "center",
        alignItems: "center",
        marginTop: 5,
        marginBottom: 5,
        padding: 5,
        backgroundColor: "#02ADAD",
        width: 80,
        borderRadius: 10,
        elevation: 5
    },

    moviesContainer:{
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: "white",
    },

    movie:{
        backgroundColor: "white",
        marginLeft: 10,
        marginRight: 10
    },

    movieItems:{
        flexDirection: "row",
        margin: 10,
        backgroundColor: "white"
    },


    imageContainer:{
        width: 120,
        height: 180,
        marginLeft: 5,
        marginRight: 20
    },

    Properties:{
        flexDirection: "column",
        marginTop: 10
    },

    moviesSepatator:{
        height: 1,
        width: "100%",
        backgroundColor: "#CED0CE"
    },

    startContainer:{
        marginLeft: 10,
        marginRight: 10,
        height: 400,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        elevation: 10
    }

}));