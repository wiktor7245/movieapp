import {StyleSheet} from "react-native";

export default (MovieDetailsStyles = StyleSheet.create({


    main:{
        display:"flex",
        alignItems: "center",
        justifyContent: "center"
    },

    container:{
        backgroundColor: "white",
        margin: 10,
        elevation: 10
    },

    imageContainer:{
       marginTop: 10,
       marginBottom: 10,
       alignItems:"center"
    },

    imageStyle:{
        width: 160,
        height: 220,
    }
}));