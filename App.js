import MovieDetails from "./components/MovieDetails.js";
import React from "react";
import { View } from "react-native";
import WelcomePage from "./components/WelcomePage.js";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

const MainNavigator = createStackNavigator(
  {
    Home: {
      screen: WelcomePage,
      navigationOptions: {
        header: null
      }
    },
    Profile: {
      screen: MovieDetails,
      navigationOptions: {
        headerTitle: "Movie Details",
        headerRight: <View />,
        headerStyle: {
          marginBottom: 10
        }
      }
    }
  },
  {
    cardStyle: {
      backgroundColor: "#EDEDED"
    }
  }
);

const App = createAppContainer(MainNavigator);

export default App;
