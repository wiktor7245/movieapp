import {
  Image,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import React, { Component } from "react";
import { customAlert, renderIf } from "../helpers/common";

import Loader from "./Loader";
import WelcomePageStyles from "../styles/WelcomePageStyles";
import { callRemoteMethod } from "../helpers/Connector";

class WelcomePage extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    appStart: true,
    movieList: [],
    isLoading: false,
    searchText: "",
    noData: false
  };

  searchButtonPressed = () => {
    this.setState({appStart:false})
    if (this.state.searchText.length) {
      //i know it's wrong
      const endpoint =
        `https://api.themoviedb.org/3/search/movie?api_key=3f57a4644f77070a3318b72a379f54e7&query=` +
        this.state.searchText +
        `&page=1&include_adult=true`;
      callRemoteMethod(this, endpoint, {}, "searchCallback", "GET", true);
    } else {
      customAlert("This field is required");
    }
  };

  searchCallback = response => {
    if (response.results.length) {
      this.setState({ noData: false });
      this.setState({ movieList: response.results });
    } else {
      this.setState({ movieList: [] });
      this.setState({ noData: true });
    }
  };

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={WelcomePageStyles.main}>
        {this.state.isLoading ? (
          <Loader show={true} loading={this.state.isLoading} />
        ) : null}
        <View style={WelcomePageStyles.searchBar}>
          <TextInput
            placeholder="Search"
            onChangeText={text => this.setState({ searchText: text })}
            underlineColorAndroid="transparent"
          />
          <View style={WelcomePageStyles.searchBtnContainer}>
            <TouchableOpacity onPress={() => this.searchButtonPressed()}>
              <Text>Search</Text>
            </TouchableOpacity>
          </View>
        </View>
        {renderIf(
          this.state.appStart,
          <View style={WelcomePageStyles.startContainer}>
            <Text>Use search bar to find movies.</Text>
          </View>
        )}
        {renderIf(
          this.state.noData,
          <Text style={{ textAlign: "center" }}>No data found.</Text>
        )}
        {renderIf(
          this.state.movieList.length,
          <ScrollView
            style={WelcomePageStyles.moviesContainer}
            showsVerticalScrollIndicator={true}
          >
            <View>
              {this.state.movieList.map(function(obj, i) {
                return (
                  <TouchableOpacity
                    onPress={() =>
                      navigate("Profile", {
                        id: obj.id
                      })
                    }
                    key={i}
                    style={WelcomePageStyles.movie}
                  >
                    <View style={WelcomePageStyles.movieItems}>
                      <Image
                        source={{
                          uri:
                            obj.poster_path != null
                              ? `https://image.tmdb.org/t/p/w600_and_h900_bestv2` +
                                obj.poster_path
                              : "https://s3-ap-southeast-1.amazonaws.com/popcornsg/placeholder-movieimage.png"
                        }}
                        style={WelcomePageStyles.imageContainer}
                        onError={() => alert("Failed")}
                        resizeMode={"cover"}
                      />
                      <View style={{ flexDirection: "column", flexShrink: 1 }}>
                        <Text
                          numberOfLines={1}
                          style={{ fontSize: 18, fontWeight: "bold" }}
                        >
                          {obj.original_title}
                        </Text>
                        <View style={WelcomePageStyles.Properties}>
                          <Text>{"Popularity: " + obj.popularity + "%"}</Text>
                          <Text>{"Number of votes: " + obj.vote_count}</Text>
                        </View>
                      </View>
                    </View>
                    <View style={WelcomePageStyles.moviesSepatator} />
                  </TouchableOpacity>
                );
              }, this)}
            </View>
          </ScrollView>
        )}
      </View>
    );
  }
}

export default WelcomePage;
