import { Image, ScrollView, Text, View } from "react-native";
import React, { Component } from "react";

import Loader from "./Loader";
import MovieDetailsStyles from "../styles/MovieDetailsStyles";
import { callRemoteMethod } from "../helpers/Connector";

class MovieDetails extends Component {
  state = {
    movieDetails: {},
    isLoading: false
  };

  genresArr = [];

  componentDidMount() {
    this.getMovieDetails();
  }

  getMovieDetails = () => {
    var endpoint =
      `https://api.themoviedb.org/3/movie/` +
      this.props.navigation.state.params.id +
      `?api_key=` +
      `3f57a4644f77070a3318b72a379f54e7`;
    callRemoteMethod(
      this,
      endpoint,
      {},
      "getMovieDetailsCallback",
      "GET",
      true
    );
  };

  getMovieDetailsCallback = response => {
    this.setState({ movieDetails: response });
  };

  getGenres = () => {
    for (i in this.state.movieDetails.genres) {
      if (i == 0) {
        this.genresArr[i] = this.state.movieDetails.genres[i].name;
      } else {
        this.genresArr[i] = ", " + this.state.movieDetails.genres[i].name;
      }
    }
  };

  render() {
    return (
      <View style={MovieDetailsStyles.main}>
        {this.state.isLoading ? (
          <Loader show={true} loading={this.state.isLoading} />
        ) : null}
        <ScrollView
          style={MovieDetailsStyles.container}
          showsVerticalScrollIndicator={false}
        >
          <View style={MovieDetailsStyles.imageContainer}>
            <Image
              style={MovieDetailsStyles.imageStyle}
              source={{
                uri:
                  this.state.movieDetails.poster_path != null
                    ? `http://image.tmdb.org/t/p/w185` +
                      this.state.movieDetails.poster_path
                    : `https://s3-ap-southeast-1.amazonaws.com/popcornsg/placeholder-movieimage.png`
              }}
            />
          </View>
          <View style={{ flexDirection: "column", padding: 10 }}>
            {this.getGenres()}
            <Text
              style={{ fontSize: 18, fontWeight: "bold", alignSelf: "center" }}
            >
              {this.state.movieDetails.original_title}
            </Text>
            <Text>
              {"Number of votes: " + this.state.movieDetails.vote_count}
            </Text>
            <Text>{"Popularity: " + this.state.movieDetails.popularity}</Text>
            <Text>
              {"Language: " + this.state.movieDetails.original_language}
            </Text>
            <View style={{ flexDirection: "row" }}>
              <Text>{"Genres: "}</Text>
              {this.genresArr.map((item, i) => (
                <Text key={i}>{item}</Text>
              ))}
            </View>
            <Text>{"Overview: " + this.state.movieDetails.overview}</Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default MovieDetails;
